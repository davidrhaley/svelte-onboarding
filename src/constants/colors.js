export const colors = {
    darkBlue: "#060a1b",
    darkBlue2: "#030611",
    darkGray: "#666666",
    lightGray: "#b2b3b6",
    mediumGray: "#73747b",
    mediumWhite: "#f8f8f8",
    red: "#ca2956",
    white: "#ffffff"
}

export const baseColors = {
    dark: {
        backgroundColor: colors.darkBlue,
        fontColor: colors.white,
        headingColor: colors.white,
        linkColor: colors.red
    },
    light: {
        backgroundColor: colors.mediumWhite,
        fontColor: colors.darkBlue2,
        headingColor: colors.darkBlue2,
        linkColor: colors.red
    }
};