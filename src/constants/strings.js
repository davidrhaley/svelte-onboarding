export const strings = {
    onboarding_slider: {
        headings: {
            assess: "Assess",
            educate: "Educate",
            empower: "Empower"
        },
        body: {
            assess: "Reference and training checklists provide real-time assessment and employee support",
            educate: "Assist at the point of activity with process knowledge and product information.",
            empower: "Enable your workforce to excel at their responsibilites, and provide exceptional customer service"
        },
        finish: "Finish",
        skip: "Skip Tour"
    },
    buttons: {
        about_smart_access: "About Smart Access",
        cancel: "Cancel",
        finish: "Finish",
        open_reader: "Open Reader"
    },
    nfc: {
        enable: {
            heading: "Enable NFC Reader",
            description: "Open the NFC reader on your iPhone to read the tag."
        },
        ready: {
            heading: "Ready to Scan",
            description: "Hold your device near the tag to scan it."
        },
        open: "Open Reader"
    },
    already_a_user: "Already a user?",
    login: "Login",
}