export const assets = {
    logo: {
        color: "assets/logo/color.png",
        white: "assets/logo/white.png"
    },
    illustration: {
        onboarding: {
            assess: "assets/illustration/onboarding/assess.png",
            educate: "assets/illustration/onboarding/educate.png",
            empower: "assets/illustration/onboarding/empower.png",
        },
        nfc: "assets/illustration/nfc.png",
        no_nfc: "assets/illustration/no_nfc.png",
        tap_device: "assets/illustration/tap_device.png",
    }
}
