import App from './App.svelte';
import './styles/global.css';

const appContainer = document.createElement("div");
appContainer.classList.add("app-container");

document.body.appendChild(appContainer);

const app = new App({
    target: appContainer,
});

