import { assets } from "./constants/assets";
import { strings } from "./constants/strings";

export const slides = {
    onboarding: [
        {
            imageSrc: assets.illustration.onboarding.educate,
            headingText: strings.onboarding_slider.headings.educate,
            bodyText: strings.onboarding_slider.body.educate
        },
        {
            imageSrc: assets.illustration.onboarding.assess,
            headingText: strings.onboarding_slider.headings.assess,
            bodyText: strings.onboarding_slider.body.assess
        },
        {
            imageSrc: assets.illustration.onboarding.empower,
            headingText: strings.onboarding_slider.headings.empower,
            bodyText: strings.onboarding_slider.body.empower
        }
    ],
};

export const pages = {
    main: {
        imageSrc: assets.illustration.nfc,
        headingText: strings.nfc.enable.heading,
        bodyText: strings.nfc.enable.description
    }
}