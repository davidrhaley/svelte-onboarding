// @ts-nocheck
// WebPack specific details, that we pull in here through composition
const parts = require("./webpack.parts");

// Allows us to merge parts with common configuration
const merge = require("webpack-merge");

const HtmlWebpackPlugin = require("html-webpack-plugin");

const CopyWebpackPlugin = require('copy-webpack-plugin');

const path = require('path');
const PATHS = {
    app: path.resolve(__dirname, "./src")
};

const commonConfig = merge([
    {
        entry: {
            bundle: ['./src/main.js']
        },
        output: {
            path: path.resolve('./dist'),
            filename: '[name].js',
            chunkFilename: '[name].[id].js'
        },
        resolve: {
            alias: {
                svelte: path.resolve('node_modules', 'svelte')
            },
            extensions: ['.mjs', '.js', '.svelte'],
            mainFields: ['svelte', 'browser', 'module', 'main']
        },

        target: "web",

        plugins: [
            new HtmlWebpackPlugin({
                title: "RFC Onboarding",
            }),
            new CopyWebpackPlugin([
                { from: 'src/assets', to: 'assets' }
            ])
        ],
    },

    parts.loadImages({ include: path.resolve(__dirname, 'src/assets') }),
    // parts.loadLess({ include: path.resolve(__dirname, 'public') }),
    parts.loadCSS({ include: path.resolve(__dirname, 'public') }),
    parts.loadSvelte({ include: path.resolve(__dirname, 'src') }),
]);

const productionConfig = merge([
    {
        output: {
            publicPath: './',
            path: path.resolve(__dirname, 'dist')
        },
    },
    parts.extractCSS({
        use: "css-loader",
    }),
    parts.loadPostCSS({ include: path.resolve(__dirname, 'dist')}),
    parts.clean(),
]);

const developmentConfig = merge([
    parts.devServer({
        host: process.env.HOST,
        port: process.env.PORT,
        contentBase: [
            path.join(__dirname, 'public'),
            path.join(__dirname, 'dist'),
        ],
    }),
    parts.loadCSS({}),
]);

module.exports = (mode) => {

    // Set the Babel env to match the Webpack env
    process.env.BABEL_ENV = mode;

    const config = (mode === "production") ? productionConfig : developmentConfig;
    return merge(commonConfig, config, { mode });
};