### What is this repository for? ###

This repository contains a project that uses the Svelte compiler, and demonstrates a simple user onboarding flow.

### How do I view this project? ###

Install npm dependencies: ```npm install```


#### Option 1: Build #

Build the project _(outputs to /dist)_: ```npm run build```


#### Option 2: Run the Development Server

Run the dev server: ```npm run serve```

Visit _localhost:8080_ in your browser